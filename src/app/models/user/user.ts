
export class User {
    
    name : {
        first:string;
        last: string;
    };
    
    location  : string;
    department: string;
    motto     : string;
    email     : string;
    phone     : string;
    guid      : string;
    _pair     : User;

    get fullName(): string {
        return this.name.first + ' ' + this.name.last;
    }

}