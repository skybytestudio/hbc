import { Injectable } from '@angular/core';
import { User } from '../../models/user/user';

@Injectable({
    providedIn: 'root'
})
export class CacheService {
  
    cache = new Set([]);

    constructor(){
    } 

    addToCache(user1: User, user2: User){
        this.cache.add(user1.guid+'+'+user2.guid);
    }

    getFromCache(user1: User, user2: User) : boolean{
        return this.cache.has(user1.guid+'+'+user2.guid);
    }

    resetCache(){
        this.cache = new Set([]);
    }

    debugCache(): void{
        console.log('cache = ',this.cache);
    }

}