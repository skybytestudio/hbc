import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MessageState } from './message.state';

@Injectable({
    providedIn: 'root'
})

export class MessageService {
 
    private messageSource = new Subject<MessageState>();
    currentMessage = this.messageSource.asObservable();
    
    changeMessage(message: string, week: number) {
        this.messageSource.next(<MessageState>{message:message, value:week});
    }    
}