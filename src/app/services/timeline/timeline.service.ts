import { Injectable } from '@angular/core';
import { CacheService } from '../cashe/cache.service';

@Injectable({
    providedIn: 'root'
})
export class TimelineService {

    public week: number = 0;

    constructor(private cacheService:CacheService){

    }

    gotToNextWeek(){
        this.week ++;
    }

    getWeek () : number{
        return this.week;
    }

    resetWeek (){
        this.cacheService.resetCache();
        this.week = 0;
    }

}