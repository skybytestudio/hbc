import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Users } from '../../models/user/users';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class ContentService {
  
    constructor(private http: HttpClient){
    } 

    /** GET users from the server */
    getUsers (params : HttpParams) : Observable<Users> {
        return this.http.get<Users>('https://hbc-frontend-challenge.hbccommon.private.hbc.com/coffee-week/users', {params: params});
    }


}