import { Component, OnInit } from '@angular/core';
import { ContentService } from './content.service';
import { MessageService } from '../../services/message/message.service';
import { TimelineService } from '../../services/timeline/timeline.service';
import { HttpParams } from '@angular/common/http';
import { User } from '../../models/user/user';
import { CacheService } from '../../services/cashe/cache.service';

@Component({
  selector: 'content-component',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements OnInit {

  response: {};
  currentWeek : number;
  responseStatus : string;

  constructor(
    private contentService : ContentService, 
    private timelineService:TimelineService, 
    private messageService : MessageService,
    private cacheService : CacheService
  ) {} 

  ngOnInit() {
    this.renderUsers();
    this.messageService.currentMessage.subscribe(result => {
      if(result.message == 'next week'){
        this.renderUsers();
      }
    })
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  renderUsers () : void {
    this.currentWeek = this.timelineService.getWeek();
    this.responseStatus = 'start';

    let params = new HttpParams();
    //params = params.append('department', 'engineering'); //...optional

    this.contentService.getUsers(params).subscribe(
      response => {
        //----------------------------------
        //Generate set1/set2 arrays
        // where :
        //   * set1 = array of engeeners from NY and DUB, who will look for a pair.
        //   * set2 = array of available pairs.
        //----------------------------------
        let set1 : User[] = []; 
        let set2 : User[] = [];
        response.users.forEach(user => {
          if(user.department == 'engineering' && (user.location == 'ny' || user.location == 'dub')){
            set1.push(user);
          }else{
            set2.push(user);
          }
        });

        //----------------------------------
        //Generate setToRender array, that contains only visible on screen users with pairs 
        //----------------------------------
        let setToRender : User[] = []; //this will be the set we render on the screen.
        set1.forEach(user1 => {
          let pickRandomIndex:number = this.getRandomInt(set2.length); //pick any random person from 2nd set
          let pickRandomUser = set2.splice(pickRandomIndex, 1); 
          let user2 = pickRandomUser[0]; 
          //there might be not enough users from set2 to pair with set1 if array sizes
          if(user2){
            //check if user2 is not in the cashe for user1.
            let canPair = !this.cacheService.getFromCache(user1,user2);
            if(canPair){
              user1._pair = user2;
              this.cacheService.addToCache(user1,user2);
              setToRender.push(user1);
            }
          }
        });
        
        //----------------------------------
        // output response 
        //----------------------------------
        this.cacheService.debugCache();
        if(setToRender.length > 0){
          this.response = setToRender;
          this.responseStatus = 'success';
        }else{
          this.responseStatus = 'empty';
        }
      },
      error => {
        this.responseStatus = 'error';
      }
    );
  }

}
