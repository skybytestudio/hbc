import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../services/message/message.service';

@Component({
  selector: 'header-component',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit  {

  initialDate:Date = new Date();
  nextDate:Date = new Date();

  constructor(
    private messageService : MessageService
  ) {} 

  ngOnInit() {
    this.messageService.currentMessage.subscribe(result => {
      if(result.message == 'next week'){
        let week  = result.value;
        let timestamp = this.initialDate.getTime();
        let weekTimestamp = 1000 * 60 * 60 * 24 * 7 * week;
        let nextWeek = timestamp + weekTimestamp;
        this.nextDate = new Date(nextWeek);
      }
    })
  }


}

