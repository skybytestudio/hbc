import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../services/message/message.service';
import { TimelineService } from '../../services/timeline/timeline.service';

@Component({
  selector: 'toolbar-component',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit  {
    constructor(
        private timelineService : TimelineService, 
        private messageService : MessageService
    ) {} 

    ngOnInit() {
        //logic related to component 
    }

    getNextWeekResults(){
        this.timelineService.gotToNextWeek();
        this.messageService.changeMessage('next week', this.timelineService.getWeek());
    }

    resetWeekToInitialValue (){
        this.timelineService.resetWeek();
        this.messageService.changeMessage('next week', this.timelineService.getWeek());
    }
}
